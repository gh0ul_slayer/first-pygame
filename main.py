import pygame
import math
import time
import configparser

conf = configparser.RawConfigParser()
conf_file = "./config.cfg"
conf.read(conf_file)
speed = float(conf.get("data", "speed"))
ship_speed = float(conf.get("data", "ship_speed"))
FFont = conf.get("data", "font")
pygame.init()
green = (96, 149, 0)
winner = pygame.font.Font(FFont, 100)
obj = pygame.font.Font(FFont, 40)
screen = pygame.display.set_mode((1200, 800))


pygame.display.set_caption("River Game")

# player1
player1Img = pygame.image.load('./Images/pokemon.png')
player1x = 584
player1y = 768
player1x_chng = 0
player1y_chng = 0

# player2
player2Img = pygame.image.load('./Images/pokemon1.png')
player2x = 584
player2y = 0
player2x_chng = 0
player2y_chng = 0


# ship1
ship1Img = pygame.image.load('./Images/ship.png')
ship1x = 0
ship1y = 78
ship1x_chng = 0
ship1y_chng = 0


# ship2
ship2Img = pygame.image.load('./Images/ship.png')
ship2x = 200
ship2y = 369
ship2x_chng = 0
ship2y_chng = 0

# ship3
ship3Img = pygame.image.load('./Images/shark.png')
ship3x = 105
ship3y = 514
ship3x_chng = 0
ship3y_chng = 0

# ship4
ship4Img = pygame.image.load('./Images/shark.png')
ship4x = 500
ship4y = 221
ship4x_chng = 0
ship4y_chng = 0

# ship5
ship5Img = pygame.image.load('./Images/ship.png')
ship5x = 679
ship5y = 656
ship5x_chng = 0
ship5y_chng = 0

# stone1
stone1Img = pygame.image.load('./Images/stone.png')
stone1x = 0
stone1y = 150


# stone2
stone2Img = pygame.image.load('./Images/stone.png')
stone2x = 300
stone2y = 150


# stone3
stone3Img = pygame.image.load('./Images/stone.png')
stone3x = 747
stone3y = 150

# stone4
stone4Img = pygame.image.load('./Images/stone.png')
stone4x = 134
stone4y = 294

# stone5
stone5Img = pygame.image.load('./Images/stone.png')
stone5x = 636
stone5y = 439

# stone6
stone6Img = pygame.image.load('./Images/stone.png')
stone6x = 334
stone6y = 439

# stone7
stone7Img = pygame.image.load('./Images/stone.png')
stone7x = 936
stone7y = 294

# stone8
stone8Img = pygame.image.load('./Images/stone.png')
stone8x = 402
stone8y = 439

# stone9
stone9Img = pygame.image.load('./Images/stone.png')
stone9x = 592
stone9y = 294

# stone210
stone10Img = pygame.image.load('./Images/stone.png')
stone10x = 1102
stone10y = 150

# stone11
stone11Img = pygame.image.load('./Images/stone.png')
stone11x = 184
stone11y = 584

# stone12
stone12Img = pygame.image.load('./Images/stone.png')
stone12x = 552
stone12y = 584
stone12x_chng = 0
stone12y_chng = 0
# stone13
stone13Img = pygame.image.load('./Images/stone.png')
stone13x = 836
stone13y = 584
stone13x_chng = 0
stone13y_chng = 0

# treasure
chestImg = pygame.image.load('./Images/chest.png')
chestx = 502
chesty = 439


def player1(x, y):
    screen.blit(player1Img, (x, y))


def player2(x, y):
    screen.blit(player2Img, (x, y))


def ship1(x, y):
    screen.blit(ship1Img, (x, y))


def ship2(x, y):
    screen.blit(ship2Img, (x, y))


def ship3(x, y):
    screen.blit(ship3Img, (x, y))


def ship4(x, y):
    screen.blit(ship4Img, (x, y))


def ship5(x, y):
    screen.blit(ship5Img, (x, y))


def stone1(x, y):
    screen.blit(stone1Img, (x, y))


def stone2(x, y):
    screen.blit(stone2Img, (x, y))


def stone3(x, y):
    screen.blit(stone3Img, (x, y))


def stone4(x, y):
    screen.blit(stone4Img, (x, y))


def stone5(x, y):
    screen.blit(stone5Img, (x, y))


def stone6(x, y):
    screen.blit(stone6Img, (x, y))


def stone7(x, y):
    screen.blit(stone7Img, (x, y))


def stone8(x, y):
    screen.blit(stone8Img, (x, y))


def stone9(x, y):
    screen.blit(stone9Img, (x, y))


def stone10(x, y):
    screen.blit(stone10Img, (x, y))


def stone11(x, y):
    screen.blit(stone11Img, (x, y))


def stone12(x, y):
    screen.blit(stone12Img, (x, y))


def stone13(x, y):
    screen.blit(stone13Img, (x, y))


def chest(x, y):
    screen.blit(chestImg, (x, y))


def treasure(player1x, player1y, chestx, chesty):
    distance = math.sqrt(
        math.pow(
            chestx -
            player1x,
            2) +
        math.pow(
            chesty -
            player1y,
            2))
    if distance < 50:
        return True
    else:
        return False


def iscollision1(player1x, player1y, ship1x, ship1y):
    distance = math.sqrt(
        math.pow(
            ship1x -
            player1x,
            2) +
        math.pow(
            ship1y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def iscollision2(player1x, player1y, ship2x, ship2y):
    distance = math.sqrt(
        math.pow(
            ship2x -
            player1x,
            2) +
        math.pow(
            ship2y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def iscollision3(player1x, player1y, ship3x, ship3y):
    distance = math.sqrt(
        math.pow(
            ship3x -
            player1x,
            2) +
        math.pow(
            ship3y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def iscollision4(player1x, player1y, ship4x, ship4y):
    distance = math.sqrt(
        math.pow(
            ship4x -
            player1x,
            2) +
        math.pow(
            ship4y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def iscollision5(player1x, player1y, ship5x, ship5y):
    distance = math.sqrt(
        math.pow(
            ship5x -
            player1x,
            2) +
        math.pow(
            ship5y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False

# stone collision


def stonecollision1(player1x, player1y, stone1x, stone1y):
    distance = math.sqrt(
        math.pow(
            stone1x -
            player1x,
            2) +
        math.pow(
            stone1y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stonecollision2(player1x, player1y, stone2x, stone2y):
    distance = math.sqrt(
        math.pow(
            stone2x -
            player1x,
            2) +
        math.pow(
            stone2y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stonecollision3(player1x, player1y, stone3x, stone3y):
    distance = math.sqrt(
        math.pow(
            stone3x -
            player1x,
            2) +
        math.pow(
            stone3y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stonecollision4(player1x, player1y, stone4x, stone4y):
    distance = math.sqrt(
        math.pow(
            stone4x -
            player1x,
            2) +
        math.pow(
            stone4y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stonecollision5(player1x, player1y, stone5x, stone5y):
    distance = math.sqrt(
        math.pow(
            stone5x -
            player1x,
            2) +
        math.pow(
            stone5y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stonecollision6(player1x, player1y, stone6x, stone6y):
    distance = math.sqrt(
        math.pow(
            stone6x -
            player1x,
            2) +
        math.pow(
            stone6y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stonecollision7(player1x, player1y, stone7x, stone7y):
    distance = math.sqrt(
        math.pow(
            stone7x -
            player1x,
            2) +
        math.pow(
            stone7y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stonecollision8(player1x, player1y, stone8x, stone8y):
    distance = math.sqrt(
        math.pow(
            stone8x -
            player1x,
            2) +
        math.pow(
            stone8y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stonecollision9(player1x, player1y, stone9x, stone9y):
    distance = math.sqrt(
        math.pow(
            stone9x -
            player1x,
            2) +
        math.pow(
            stone9y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stonecollision10(player1x, player1y, stone10x, stone10y):
    distance = math.sqrt(
        math.pow(
            stone10x -
            player1x,
            2) +
        math.pow(
            stone10y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stonecollision11(player1x, player1y, stone11x, stone11y):
    distance = math.sqrt(
        math.pow(
            stone11x -
            player1x,
            2) +
        math.pow(
            stone11y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stonecollision12(player1x, player1y, stone12x, stone12y):
    distance = math.sqrt(
        math.pow(
            stone12x -
            player1x,
            2) +
        math.pow(
            stone12y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stonecollision13(player1x, player1y, stone13x, stone13y):
    distance = math.sqrt(
        math.pow(
            stone13x -
            player1x,
            2) +
        math.pow(
            stone13y -
            player1y,
            2))
    if distance < 48:
        return True
    else:
        return False
# .asdfasdfASDFADFADFADFADFADFASDFASDFADFASDF
# asdfasdfASDFADFADFADFADFADFASDFASDFADFASDFADFASDF
# asdfasdfASDFADFADFADFADFADFASDFASDFADFASDFADFASDFSDFASD
# FASD
# FASDFASDF


def is1collision1(player2x, player2y, ship1x, ship1y):
    distance = math.sqrt(
        math.pow(
            ship1x -
            player2x,
            2) +
        math.pow(
            ship1y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def is2collision2(player2x, player2y, ship2x, ship2y):
    distance = math.sqrt(
        math.pow(
            ship2x -
            player2x,
            2) +
        math.pow(
            ship2y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def is3collision3(player2x, player2y, ship3x, ship3y):
    distance = math.sqrt(
        math.pow(
            ship3x -
            player2x,
            2) +
        math.pow(
            ship3y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def is4collision4(player2x, player2y, ship4x, ship4y):
    distance = math.sqrt(
        math.pow(
            ship4x -
            player2x,
            2) +
        math.pow(
            ship4y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def is5collision5(player2x, player2y, ship5x, ship5y):
    distance = math.sqrt(
        math.pow(
            ship5x -
            player2x,
            2) +
        math.pow(
            ship5y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False

# stone collision


def stone2collision1(player2x, player2y, stone1x, stone1y):
    distance = math.sqrt(
        math.pow(
            stone1x -
            player2x,
            2) +
        math.pow(
            stone1y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stone2collision2(player2x, player2y, stone2x, stone2y):
    distance = math.sqrt(
        math.pow(
            stone2x -
            player2x,
            2) +
        math.pow(
            stone2y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stone2collision3(player2x, player2y, stone3x, stone3y):
    distance = math.sqrt(
        math.pow(
            stone3x -
            player2x,
            2) +
        math.pow(
            stone3y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stone2collision4(player2x, player2y, stone4x, stone4y):
    distance = math.sqrt(
        math.pow(
            stone4x -
            player2x,
            2) +
        math.pow(
            stone4y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stone2collision5(player2x, player2y, stone5x, stone5y):
    distance = math.sqrt(
        math.pow(
            stone5x -
            player2x,
            2) +
        math.pow(
            stone5y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stone2collision6(player2x, player2y, stone6x, stone6y):
    distance = math.sqrt(
        math.pow(
            stone6x -
            player2x,
            2) +
        math.pow(
            stone6y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stone2collision7(player2x, player2y, stone7x, stone7y):
    distance = math.sqrt(
        math.pow(
            stone7x -
            player2x,
            2) +
        math.pow(
            stone7y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stone2collision8(player2x, player2y, stone8x, stone8y):
    distance = math.sqrt(
        math.pow(
            stone8x -
            player2x,
            2) +
        math.pow(
            stone8y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stone2collision9(player2x, player2y, stone9x, stone9y):
    distance = math.sqrt(
        math.pow(
            stone9x -
            player2x,
            2) +
        math.pow(
            stone9y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stone2collision10(player2x, player2y, stone10x, stone10y):
    distance = math.sqrt(
        math.pow(
            stone10x -
            player2x,
            2) +
        math.pow(
            stone10y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stone2collision11(player2x, player2y, stone11x, stone11y):
    distance = math.sqrt(
        math.pow(
            stone11x -
            player2x,
            2) +
        math.pow(
            stone11y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stone2collision12(player2x, player2y, stone12x, stone12y):
    distance = math.sqrt(
        math.pow(
            stone12x -
            player2x,
            2) +
        math.pow(
            stone12y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


def stone2collision13(player2x, player2y, stone13x, stone13y):
    distance = math.sqrt(
        math.pow(
            stone13x -
            player2x,
            2) +
        math.pow(
            stone13y -
            player2y,
            2))
    if distance < 48:
        return True
    else:
        return False


run = True


c = 1
l1 = True
l2 = True
count1 = 0
count2 = 0
level = 0
var1 = 0
var2 = 0
score1 = 0
score2 = 0
atime = 0
power = True
p1time = 0
p2time = 0
# game loop

while run:

    screen.fill((0, 130, 255))

    pygame.draw.rect(screen, green, (0, 0, 1200, 75))
    pygame.draw.rect(screen, green, (0, 145, 1200, 75))
    pygame.draw.rect(screen, green, (0, 290, 1200, 75))
    pygame.draw.rect(screen, green, (0, 435, 1200, 75))
    pygame.draw.rect(screen, green, (0, 580, 1200, 75))
    pygame.draw.rect(screen, green, (0, 725, 1200, 75))

    if c == 1 and l1:
        player1(player1x, player1y)
        atime = pygame.time.get_ticks()
    elif c == 2 and l2:
        player2(player2x, player2y)
        atime = pygame.time.get_ticks()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_e:
                run = False

        if c == 1:

            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_LEFT:
                    player1x_chng = -speed
                elif event.key == pygame.K_RIGHT:
                    player1x_chng = speed
                elif event.key == pygame.K_DOWN:
                    player1y_chng = speed
                elif event.key == pygame.K_UP:
                    player1y_chng = -speed

            elif event.type == pygame.KEYUP:

                if event.key == pygame.K_LEFT:
                    player1x_chng = 0
                elif event.key == pygame.K_RIGHT:
                    player1x_chng = 0
                if event.key == pygame.K_DOWN:
                    player1y_chng = 0
                elif event.key == pygame.K_UP:
                    player1y_chng = 0

        if c == 2:

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    player2x_chng = -speed
                elif event.key == pygame.K_d:
                    player2x_chng = speed
                elif event.key == pygame.K_s:
                    player2y_chng = speed
                elif event.key == pygame.K_w:
                    player2y_chng = -speed
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_a:
                    player2x_chng = 0
                elif event.key == pygame.K_d:
                    player2x_chng = 0
                if event.key == pygame.K_s:
                    player2y_chng = 0
                elif event.key == pygame.K_w:
                    player2y_chng = 0

    if c == 1:
        player1x += player1x_chng
        if player1x <= 0:
            player1x = 0
        elif player1x >= 1168:
            player1x = 1168

        player1y += player1y_chng
        if player1y <= 0:
            player1y = 0
        elif player1y >= 768:
            player1y = 768

    if c == 2:
        player2x += player2x_chng
        if player2x <= 0:
            player2x = 0
        elif player2x >= 1168:
            player2x = 1168

        player2y += player2y_chng
        if player2y <= 0:
            player2y = 0
        elif player2y >= 768:
            player2y = 768

    ship1x += ship_speed
    if ship1x >= 1200:
        ship1x = -64
    ship2x += ship_speed
    if ship2x >= 1200:
        ship2x = -64
    ship3x += ship_speed
    if ship3x >= 1200:
        ship3x = -64
    ship4x += ship_speed
    if ship4x >= 1200:
        ship4x = -64
    ship5x += ship_speed
    if ship5x >= 1200:
        ship5x = -64

    stone1(stone1x, stone1y)
    stone2(stone2x, stone2y)
    stone3(stone3x, stone3y)
    stone4(stone4x, stone4y)
    stone5(stone5x, stone5y)
    stone6(stone6x, stone6y)
    stone7(stone7x, stone7y)
    stone8(stone8x, stone8y)
    stone9(stone9x, stone9y)
    stone10(stone10x, stone10y)
    stone11(stone11x, stone11y)
    stone12(stone12x, stone12y)
    stone13(stone13x, stone13y)

    ship1(ship1x, ship1y)
    ship2(ship2x, ship2y)
    ship3(ship3x, ship3y)
    ship4(ship4x, ship4y)
    ship5(ship5x, ship5y)

    if c == 1:
        # ship collision
        collision1 = iscollision1(player1x, player1y, ship1x, ship1y)
        if collision1:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        collision2 = iscollision2(player1x, player1y, ship2x, ship2y)
        if collision2:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        collision3 = iscollision3(player1x, player1y, ship3x, ship3y)
        if collision3:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        collision4 = iscollision4(player1x, player1y, ship4x, ship4y)
        if collision4:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        collision5 = iscollision5(player1x, player1y, ship5x, ship5y)
        if collision5:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        # stone collision
        fixedcollision1 = stonecollision1(player1x, player1y, stone1x, stone1y)
        if fixedcollision1:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        fixedcollision2 = stonecollision1(player1x, player1y, stone2x, stone2y)
        if fixedcollision2:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        fixedcollision3 = stonecollision1(player1x, player1y, stone3x, stone3y)
        if fixedcollision3:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        fixedcollision4 = stonecollision1(player1x, player1y, stone4x, stone4y)
        if fixedcollision4:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        fixedcollision5 = stonecollision1(player1x, player1y, stone5x, stone5y)
        if fixedcollision5:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        fixedcollision6 = stonecollision1(player1x, player1y, stone6x, stone6y)
        if fixedcollision6:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        fixedcollision7 = stonecollision1(player1x, player1y, stone7x, stone7y)
        if fixedcollision7:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        fixedcollision8 = stonecollision1(player1x, player1y, stone8x, stone8y)
        if fixedcollision8:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        fixedcollision9 = stonecollision1(player1x, player1y, stone9x, stone9y)
        if fixedcollision9:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        fixedcollision10 = stonecollision1(
            player1x, player1y, stone10x, stone10y)
        if fixedcollision10:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        fixedcollision11 = stonecollision1(
            player1x, player1y, stone11x, stone11y)
        if fixedcollision11:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        fixedcollision12 = stonecollision1(
            player1x, player1y, stone12x, stone12y)
        if fixedcollision12:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

        fixedcollision13 = stonecollision1(
            player1x, player1y, stone13x, stone13y)
        if fixedcollision13:
            l1 = False
            l2 = True
            c = 2
            count1 = 1
            score1 += var1
            power = True
            p1time = atime

    if c == 2:
        collision12 = iscollision1(player2x, player2y, ship1x, ship1y)
        if collision12:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time
        collision22 = iscollision2(player2x, player2y, ship2x, ship2y)
        if collision22:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        collision32 = iscollision3(player2x, player2y, ship3x, ship3y)
        if collision32:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        collision42 = iscollision4(player2x, player2y, ship4x, ship4y)
        if collision42:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        collision52 = iscollision5(player2x, player2y, ship5x, ship5y)
        if collision52:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        col1 = stone2collision1(player2x, player2y, stone1x, stone1y)
        if col1:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        col2 = stone2collision1(player2x, player2y, stone2x, stone2y)
        if col2:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        col3 = stone2collision1(player2x, player2y, stone3x, stone3y)
        if col3:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        col4 = stone2collision1(player2x, player2y, stone4x, stone4y)
        if col4:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        col5 = stone2collision1(player2x, player2y, stone5x, stone5y)
        if col5:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        col6 = stone2collision1(player2x, player2y, stone6x, stone6y)
        if col6:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        col7 = stone2collision1(player2x, player2y, stone7x, stone7y)
        if col7:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        col8 = stone2collision1(player2x, player2y, stone8x, stone8y)
        if col8:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        col9 = stone2collision1(player2x, player2y, stone9x, stone9y)
        if col9:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        col10 = stone2collision1(player2x, player2y, stone10x, stone10y)
        if col10:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        col11 = stone2collision1(player2x, player2y, stone11x, stone11y)
        if col11:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        col12 = stone2collision1(player2x, player2y, stone12x, stone12y)
        if col12:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

        col13 = stone2collision1(player2x, player2y, stone13x, stone13y)
        if col13:
            l2 = False
            l1 = True
            c = 1
            count2 = 1
            score2 += var2
            power = True
            p2time = atime - p1time

    powerup1 = 0
    powerup2 = 0
    if treasure(player1x, player1y, chestx, chesty) and power:
        powerup1 = 3
        power = False
    if treasure(player2x, player2y, chestx, chesty) and power:
        powerup2 = 3
        power = False
    score1 += powerup1
    score2 += powerup2
    if player1y < 5:
        score1 += var1
        player1y = 768
        player1x = 564
        player1x_chng = 0
        player1y_chng = 0
        power = True
        if l2:
            c = 2
        else:
            c = 1
            ship_speed *= 3

    if player2y > 765:
        score2 += var2
        player2y = 0
        player2x = 564
        player2x_chng = 0
        player2y_chng = 0
        power = True
        if l1:
            c = 1
        else:
            c = 2
            ship_speed *= 3

    dlf = obj.render("Press 'e' to exit", 1, (0, 0, 0))
    if count2 == 1 and count1 == 1:
        l1 = False
        l2 = False
        screen.blit(dlf, (500, 500))
        if score1 > score2:
            win = winner.render("Winner is player1", 1, (0, 0, 0))
            ship_speed = 0
        elif score2 > score1:
            win = winner.render("Winner is player2", 1, (0, 0, 0))
            ship_speed = 0
        elif p1time < p2time:
            win = winner.render("Winner is player1", 1, (0, 0, 0))
            ship_speed = 0
        elif p2time < p1time:
            win = winner.render("Winner is player2", 1, (0, 0, 0))
            ship_speed = 0
        else:
            win = winner.render("This is a draw", 1, (0, 0, 0))
            ship_speed = 0
        screen.blit(win, (300, 370))

    if player1y < 655 and player1y >= 580:
        var1 = 10
    elif player1y < 580 and player1y >= 510:
        var1 = 15
    elif player1y < 510 and player1y >= 435:
        var1 = 25
    elif player1y < 435 and player1y >= 365:
        var1 = 30
    elif player1y < 365 and player1y >= 290:
        var1 = 40
    elif player1y < 290 and player1y >= 220:
        var1 = 45
    elif player1y < 220 and player1y >= 145:
        var1 = 55
    elif player1y < 145 and player1y >= 75:
        var1 = 60

    if player2y < 725 and player2y >= 655:
        var2 = 60
    if player2y < 655 and player2y >= 580:
        var2 = 55
    if player2y < 580 and player2y >= 510:
        var2 = 45
    if player2y < 510 and player2y >= 435:
        var2 = 40
    if player2y < 435 and player2y >= 365:
        var2 = 30
    if player2y < 365 and player2y >= 290:
        var2 = 25
    if player2y < 290 and player2y >= 220:
        var2 = 15
    if player2y < 220 and player2y >= 145:
        var2 = 10

    psc1 = obj.render("Score1 :" + str(score1), 1, (255, 160, 0))
    psc2 = obj.render("Score2 :" + str(score2), 1, (255, 160, 0))
    time1 = obj.render("P1 Time :" + str(p1time / 1000), 1, (255, 160, 0))
    time = obj.render("Total Time :" + str(atime / 1000), 1, (255, 160, 0))
    time2 = obj.render("P2 Time :" + str(p2time / 1000), 1, (255, 160, 0))

    if l1 or l2:
        screen.blit(psc1, (0, 0))
        screen.blit(psc2, (950, 0))
        screen.blit(time1, (0, 725))
        screen.blit(time2, (200, 725))
        screen.blit(time, (400, 725))

    if power:
        chest(chestx, chesty)

    pygame.display.flip()
